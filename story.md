# Holy Crap

"Hello, and welcome to the Gates", said the old man.

"Excuse me?", I replied.

"Welcome to the Gates.", the old man repeated.

"Gates? You mean the TV-show 'The Gates'? Is this a dream?"

"No, no." said the old man, smiling. "This is the Pearly Gates. Clearly,
the transition was very confusing to you. Don't worry, that's perfectly
normal."

"What??? Pearly Gates? As in Heaven?"

"Yes, as in Heaven. Now, if you please enter that line over there, you'll
be registered and given your change of robes."

"Wait... I can't be in Heaven. I'm an atheist. I don't believe in any of
this. I'm dreaming, right?"

"No, you're not dreaming. You're dead. And in Heaven. Now, please, move
over there. We have a schedule to keep and you're slowing us down now."

"Hold on a goddamned minute. Are you telling me that Heaven and God and
all that stuff is all true?"

"Well, you're in Heaven now, aren't you? Now, PLEASE, move to that line
over there, pronto." The old man was clearly upset now.

"Can I see him? It's a he, right? Can I have a chat with him, I mean, in
person?"

"Yes, you may. Look, if I schedule you in for an audience with him, will
you stop asking so many questions and move to the registration line at
once?"

"Registration line? What line?"

"THAT line!" the old man said, in an angry tone. "Now, yes or no?""Yes,
yes, I'll move to the line. So, when can I see him?"

"Let me see here. Sorry, our computer system is a bit ancient and slow.
God can see you three eons from now."

"Eons? Wait... eons are infinite. Are you telling me that I'll have to
wait an infinite amount of time to talk to God?"

"Yes, but worry you not. Time passes very quickly up here. Now, for the
last time, please step into the registration line."

"Ok, ok. You have an infinite amount of time up here at your disposal, you
know. No need to rush people. Jesus!"

Three eons later, I got a text message on my cell phone reminding me that
my time to see the big guy had arrived. Funny, I don't remember having
been given a cell phone here, and it's nothing like any cell phone I have
ever seen. Think Warehouse 13's Farnsworth, only smaller than the latest
iPod Nano.

The shuttle arrived a fraction of an eon later. Yes, believe it or not,
time in Heaven is measured in fractions of an infinite amount. If you know
anything about mathematics, specifically about transfinite numbers, then
you know how ridiculous that sounds. Anyway, the shuttle to take me to
what I can only refer to as the Throne Palace stopped in front of me.

Suddenly, I found myself sitting inside, but I don't recall stepping into
it through any door or anything. I really don't like not remembering
chunks of time, especially if they're infinite in extent. For instance, I
don't remember anything at all from the three eons that passed since I had
my little chat with Peter, the old man at the Gates.

"So, you wanted to see me.", said the booming voice in the sky. Yes,
Heaven itself also has a sky, but no stars and no sun, since there's no
day or night. It's always a homogeneously white expanse. Come to think of
it, it's much like the "ground" we glide over. Heaven is a terribly boring
place, so it seems.

"So, you wanted to see me.", repeated the booming voice.

"Well, yes, but it looks like all I can do is hear you, since all I see,
everywhere, is just white. You need a decorator, you know."

"Is that why you wanted to see me? To recommend I hire a decorator?"

"No, no. I wanted to see if you're real, and the real deal. How do I know
that you're not just a very powerful alien, rather than an actual god?"

"You don't."

"That's it? Is that the best you can offer me?"

"What else do you need?"

"I need proof, proof that you're God, or at least, a god."

"But I'm not."

"What do you mean? You're not a god?"

"No, I am a god. Just not God."

"You're deliberately confusing me. I thought you were supposed to
enlighten people, after they arrive in Heaven. So, are you or are you not
God?"

"I'm not."

"I'm still confused."

"It's very simple, really. You're an atheist, right?"

"Yes, I was, when I was alive."

"And atheists don't go to Heaven. They go to Hell, where they're punished
for not believing in God. With me, so far?"

"Yes, I'm dead, not stupid."

"So, what's the most painful punishment there is for an atheist?
Obviously, it is-" "To make the atheist believe he's in Heaven, where
he'll suffer for all eternity the humiliation of being wrong.", I finished
it for him. "So, I'm in Hell, not in Heaven, and you're not God, but the
Devil. Is that it?

"Bingo!"

I mused over this for an eon or two, then all I could think was: "Holy
crap, I'm screwed."
